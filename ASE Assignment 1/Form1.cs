﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace ASE_Assignment_1
{
    public partial class Form1 : Form
    {
        
        private int MAX = 256; //max interations
        private double SX = -2.025; //start value real
        private double SY = -1.125; //start value imaginary
        private double EX = 0.6; //end value real
        private double EY = 1.125; //end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished;
        private static float xy;
        private Bitmap picture;
        private Bitmap pict_rect;    
        private Bitmap pict_merge;
        private Graphics g1;
        private Graphics g2;
        private Graphics g3;
        private HSBColor hsb;
        //this will allow the colour of the fractal to be changed 
        int fractal_colour = 0;
        Pen Pen;
        Pen whitePen = new Pen(Color.White);

        //little comment made
       
        public struct HSBColor
        {
            float h;
            float s;
            float b;
            int a;
            public HSBColor(float h, float s, float b)
            {
                this.a = 0xff;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }
            public HSBColor(int a, float h, float s, float b)
            {
                this.a = a;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }
            public float H
            {
                get { return h; }
            }
            public float S
            {
                get { return s; }
            }
            public float B
            {
                get { return b; }
            }
            public int A
            {
                get { return a; }
            }
            public Color Color
            {
                get
                {
                    return FromHSB(this);
                }
            }

            public static Color FromHSB(HSBColor hsbColor)
            {
                float r = hsbColor.b;
                float g = hsbColor.b;
                float b = hsbColor.b;
                if (hsbColor.s != 0)
                {
                    float max = hsbColor.b;
                    float dif = hsbColor.b * hsbColor.s / 255f;
                    float min = hsbColor.b - dif;
                    float h = hsbColor.h * 360f / 255f;

                    if (h < 60f)
                    {
                        r = max;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h < 180f)
                    {
                        r = min;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = min;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = min;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                }
                return Color.FromArgb
                    (
                        hsbColor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            } //end of Colour fromHSB
        } //end of HSBColour

        public Form1()
        {
            InitializeComponent();
            init();
            start();
        }

        public void init()
        {
            new Size(640, 480);
            finished = false;
            rectangle = false;
            x1 = pictureBox.Width;
            y1 = pictureBox.Height;
            xy = (float)x1 / (float)y1;

            //bitmap to draw the mandelbrot on (original)
            picture = new Bitmap(x1, y1);
            g1 = Graphics.FromImage(picture);

            //bitmap to draw the rectangle on to zoom
            pict_rect = new Bitmap(x1, y1);
            g2 = Graphics.FromImage(pict_rect);

            //bitmap to merge the zoombox on, will redraw the mandelbrot
            pict_merge = new Bitmap(x1, y1);
            g3 = Graphics.FromImage(pict_merge);
            merge();

            pictureBox.Image = pict_merge;
            finished = true;
        } //end of init

        public void merge()
        {
            //merges the bitmaps together, so the rectangle can drawn and the mandelbrot can be redrawn after zoom
            g3.CompositingMode = CompositingMode.SourceOver;

            g3.DrawImage(picture, 0, 0);

            if (rectangle)
            {
                g3.DrawImage(pict_rect, 0, 0);
            }
            Refresh();
            
        } //end of merge



        public void destroy()
        {
            if (finished)
            {
                picture = null;
                pict_rect = null;
                pict_merge = null;
                g1 = null;
                g2 = null;
                g3 = null;
                GC.Collect(2, GCCollectionMode.Optimized);
            }
        } //end of destroy

        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot(); //calling mandelbrot to draw the fractal
        } //end of start

        public void stop()
        {
        } //end of stop

        public void paint()
        {
            update();
            merge();
        } //end of paint
        
        public void update()
        {

            
            g2.Clear(Color.Transparent) ;
            if (rectangle)
            {
                g3.DrawImage(picture, 0, 0);
                //possibly try g3 instead of bitmap image
                //g2.DrawLine(Pen, 0, 0, 0, 0);

                if (xs < xe)
                {
                    if (ys < ye) g2.DrawRectangle(whitePen,xs, ys, (xe - xs), (ye - ys));
                    else g2.DrawRectangle(whitePen,xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) g2.DrawRectangle(whitePen,xe, ys, (xs - xe), (ye - ys));
                    else g2.DrawRectangle(whitePen,xe, ye, (xs - xe), (ys - ye));
                }

                merge();
            }
            
        } //end of update

        

        private void mandelbrot()
        {
            int x, y;
            float h, b, alt = 0.0f;

            action = false;
            Cursor = Cursors.Cross;
            
            textBox.Text = ("Mandelbrot - Set will be produced - please wait...");
            //refreshing the textbox as the text was not changing when zooming 
            textBox.Refresh();

            for (x = 0; x < x1; x+=2)
                for(y =0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + +yzoom * (double)y); //colour value
                    if (h != alt)
                    {
                        b = 1.0f - h * h; //brightness

                        hsb = new HSBColor(h * 255, 0.8f * 255, b * 255);
                        Pen = new Pen(hsb.Color, 1);
                        alt = h;

                    }
                    g1.DrawLine(Pen, x, y, x + 1, y);
                }
            textBox.Text = ("Mandelbrot-Set ready - please select zoom area with pressed mouse.");

            action = true;

            merge();

        } //end of Mandelbrot

        private float pointcolour (double xwert, double ywert)
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = fractal_colour;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        } //end of pointcolour 

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //changes the fractal colour to blue
            fractal_colour = 171;
            mandelbrot();
        }

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //changes the fractal colour to red 
            fractal_colour = -15;
            mandelbrot();
        }

        private void yellowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //changes the fractal colour to yellow
            fractal_colour = 42;
            mandelbrot();
        }

        private void greenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //changes the fractal colour to green
            fractal_colour = 85;
            mandelbrot();
        }

        private void aquaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //changes the fractal colour to aqua
            fractal_colour = 128;
            mandelbrot();
        }

        private void purpleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //changes the fractal colour to purple
            fractal_colour = 213;
            mandelbrot();
        }

        private void originalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //changes the fractal colour back to normal
            fractal_colour = 0;
            mandelbrot();
        }

        private void initvalues() //reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        } //end of initvalues()

        private void pictureBox_MouseClick(object sender, MouseEventArgs e)
         {
            
         } //end of pictureBox_MouseClick

        private void pictureBox_MouseEnter(object sender, EventArgs e)
        {

        } //end of pictureBox_MouseEnter

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (action)
            {
                xe = e.X;
                ye = e.Y;
               // rectangle = true;
                update();
                Refresh();
            }
        }

        private void pictureBox_MouseLeave(object sender, EventArgs e)
        {

        } //end of pictureBox_MouseLeave

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            int z, w;

            //putting the rectangle as clear until drawn
            g2.Clear(Color.Transparent);

            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;
                Refresh();
            }
        } //end of pictureBox_MouseUp

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            rectangle = true;
            if (action)
            {
                xs = e.X;
                ys = e.Y;
            }
        } //end of pictureBox_MouseDown

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        } //end of MenuStrip

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //allows you to save the image as a jpeg
            SaveFileDialog dialog = new SaveFileDialog();
            //chooses the file type for you 
            dialog.Filter = ("JPEG (Image Type)|*.JPEG");
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                picture.Save(dialog.FileName, ImageFormat.Jpeg);
                //saves the picture
            }

        } //end of saveAs 

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = ("Text File |*.txt");
            //specifiying which type of file will be opened

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                Console.WriteLine(dialog.FileName);

                try
                {
                    //creating a string for the text to be saved to 
                    string file_text = System.IO.File.ReadAllText(dialog.FileName);
                    StringReader read_line = new StringReader(file_text);

                    //each variable is saved to a specific line
                    xstart = Double.Parse(read_line.ReadLine());
                    Console.WriteLine(xstart);

                    ystart = Double.Parse(read_line.ReadLine());
                    Console.WriteLine(ystart);

                    xzoom = Double.Parse(read_line.ReadLine());
                    Console.WriteLine(xzoom);

                    yzoom = Double.Parse(read_line.ReadLine());
                    Console.WriteLine(yzoom);

                    xende = Double.Parse(read_line.ReadLine());
                    Console.WriteLine(xende);

                    yende = Double.Parse(read_line.ReadLine());
                    Console.WriteLine(yende);

                    //this opens the fractal colour that it was when we saved it 
                    fractal_colour = Int32.Parse(read_line.ReadLine());
                    Console.WriteLine(fractal_colour);

                    //the file should open where it was last saved 

                }
                catch
                {
                    //an error message just in case it doesn't work 
                    Console.WriteLine("The File could not be opened.");
                }
            }
            //recalling the mandelbrot method so it can be redrawn 
            mandelbrot();
        } //end of openTool

        private void saveAsStateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = ("Text File |*.txt");
            //saving the file a text file -it is already chosen for them

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(dialog.FileName))
                {
                    //writing the file to a text so it can be saved 
                    file.WriteLine(xstart);
                    file.WriteLine(ystart);
                    file.WriteLine(xzoom);
                    file.WriteLine(yzoom);
                    file.WriteLine(xende);
                    file.WriteLine(yende);
                    //this saves the correct colour that we changed the fractal too, so when its open it goes back to that colour
                    file.WriteLine(fractal_colour);

                    file.Flush();
                    file.Dispose();

                }
            }
        } //end of saveAsState

        public String getAppletInfo()
        {
            return "fractal.class - Mandelbrot Set a Java Applet by Eckhard Roessel 2000-2001";
        } //end of getApplet

    } //end of partial class 
} //end of namespace
